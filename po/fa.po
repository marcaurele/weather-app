# Persian translation for ubuntu-weather-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-weather-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-weather-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-19 20:23+0000\n"
"PO-Revision-Date: 2020-02-15 06:37+0000\n"
"Last-Translator: HSN80 <hosseinabbasi80@outlook.com>\n"
"Language-Team: Persian <https://translate.ubports.com/projects/ubports/"
"weather-app/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2017-04-27 05:56+0000\n"

#: ../app/components/About.qml:27
msgid "About"
msgstr ""

#: ../app/components/About.qml:33
msgctxt "about page, section header, full changelog"
msgid "Changelog"
msgstr ""

#: ../app/components/About.qml:33
msgctxt "about page, section header, general app info"
msgid "General"
msgstr ""

#: ../app/components/About.qml:33
msgctxt "about page, section header, troubleshooting"
msgid "Troubleshooting"
msgstr ""

#: ../app/components/About.qml:33
msgctxt "about page, section header, usage instructions"
msgid "Usage"
msgstr ""

#: ../app/components/AboutChangelog.qml:61
msgctxt "changelog"
msgid ""
"improved: in location search show admin3 name, if name+admin1+admin2 yield "
"multiple identical results"
msgstr ""

#: ../app/components/AboutChangelog.qml:62
#: ../app/components/AboutChangelog.qml:69
#: ../app/components/AboutChangelog.qml:85
#: ../app/components/AboutChangelog.qml:92
#: ../app/components/AboutChangelog.qml:98
#: ../app/components/AboutChangelog.qml:106
#: ../app/components/AboutChangelog.qml:116
#: ../app/components/AboutChangelog.qml:128
#: ../app/components/AboutChangelog.qml:135
#: ../app/components/AboutChangelog.qml:143
#: ../app/components/AboutChangelog.qml:170
#: ../app/components/AboutChangelog.qml:180
#: ../app/components/AboutChangelog.qml:186
#: ../app/components/AboutChangelog.qml:207
#: ../app/components/AboutChangelog.qml:213
#: ../app/components/AboutChangelog.qml:221
#: ../app/components/AboutChangelog.qml:242
#: ../app/components/AboutChangelog.qml:249
#: ../app/components/AboutChangelog.qml:256
#: ../app/components/AboutChangelog.qml:262
#: ../app/components/AboutChangelog.qml:277
#: ../app/components/AboutChangelog.qml:293
#: ../app/components/AboutChangelog.qml:299
#: ../app/components/AboutChangelog.qml:320
#: ../app/components/AboutChangelog.qml:376
#: ../app/components/AboutChangelog.qml:388
msgctxt "changelog"
msgid "updated: translations, many thanks to all translators!"
msgstr ""

#: ../app/components/AboutChangelog.qml:68
msgctxt "changelog"
msgid "added: troubleshooting section in about page"
msgstr ""

#: ../app/components/AboutChangelog.qml:75
msgctxt "changelog"
msgid "fixed: #75 GPS based location not working"
msgstr ""

#: ../app/components/AboutChangelog.qml:76
msgctxt "changelog"
msgid ""
"fixed: #86 locale city names and description not working for some languages"
msgstr ""

#: ../app/components/AboutChangelog.qml:77
msgctxt "changelog"
msgid "fixed: wind speed unit m/s not using the locales decimal separator"
msgstr ""

#: ../app/components/AboutChangelog.qml:78
msgctxt "changelog"
msgid "fixed: none or wrong default values used for some settings"
msgstr ""

#: ../app/components/AboutChangelog.qml:79
msgctxt "changelog"
msgid ""
"fixed: not working check for am/pm time formats to allow more width for "
"hourly forecast"
msgstr ""

#: ../app/components/AboutChangelog.qml:80
msgctxt "changelog"
msgid ""
"added: chance of precipitation to hourly and daily forecast (where "
"available), thanks to openweathermap.org for adding this to the API"
msgstr ""

#: ../app/components/AboutChangelog.qml:81
msgctxt "changelog"
msgid "added: l/m² as unit for rain"
msgstr ""

#: ../app/components/AboutChangelog.qml:82
msgctxt "changelog"
msgid ""
"improved: internally rework units to allow more flexible translation e.g. "
"regarding space between number and unit"
msgstr ""

#: ../app/components/AboutChangelog.qml:83
msgctxt "changelog"
msgid ""
"improved: redesign about page now with 'general', 'usage' and 'changelog' "
"subpages"
msgstr ""

#: ../app/components/AboutChangelog.qml:84
msgctxt "changelog"
msgid ""
"removed: https url dispatcher for OWM since this conflicts with other apps"
msgstr ""

#: ../app/components/AboutChangelog.qml:91
msgctxt "changelog"
msgid "added: link to full changelog in about page"
msgstr ""

#: ../app/components/AboutChangelog.qml:104
msgctxt "changelog"
msgid "fixed: #83 automatic data refresh not working"
msgstr ""

#: ../app/components/AboutChangelog.qml:105
msgctxt "changelog"
msgid ""
"added: new icons for snow and sleet based on downfall volume to hourly and "
"daily forecasts"
msgstr ""

#: ../app/components/AboutChangelog.qml:112
msgctxt "changelog"
msgid "fixed: #78 inconsistent am/pm notation for hourly and suntimes"
msgstr ""

#: ../app/components/AboutChangelog.qml:113
msgctxt "changelog"
msgid "fixed: hourly forecast missing am/pm notations for some locales"
msgstr ""

#: ../app/components/AboutChangelog.qml:114
msgctxt "changelog"
msgid ""
"improved: date label in daily forecast can take up more space to allow more "
"localized dates to be fully displayed"
msgstr ""

#: ../app/components/AboutChangelog.qml:115
msgctxt "changelog"
msgid "updated: Momentjs with the included IANA timezone database (now v2020a)"
msgstr ""

#: ../app/components/AboutChangelog.qml:122
msgctxt "changelog"
msgid ""
"fixed: #80 now correct wind speeds are displayed with current values (before "
"daily values)"
msgstr ""

#: ../app/components/AboutChangelog.qml:123
msgctxt "changelog"
msgid ""
"fixed: #81 small redesign for visual separation of current data and day data"
msgstr ""

#: ../app/components/AboutChangelog.qml:124
msgctxt "changelog"
msgid ""
"fixed: #82 implement new forecast rain icons for hourly and daily forecast "
"(thanks @cibersheep for the icons)"
msgstr ""

#: ../app/components/AboutChangelog.qml:125
msgctxt "changelog"
msgid "added: current cloud coverage value"
msgstr ""

#: ../app/components/AboutChangelog.qml:126
msgctxt "changelog"
msgid "improved: with m/s as unit for wind speed, now 1 decimal is given"
msgstr ""

#: ../app/components/AboutChangelog.qml:127
msgctxt "changelog"
msgid "improved: vertical alignment of daily forecast information"
msgstr ""

#: ../app/components/AboutChangelog.qml:134
msgctxt "changelog"
msgid "fixed: some error messages with upcoming Qt5.12 (thanks @dobey)"
msgstr ""

#: ../app/components/AboutChangelog.qml:141
msgctxt "changelog"
msgid "fixed: some translations not using abbreviated units"
msgstr ""

#: ../app/components/AboutChangelog.qml:142
msgctxt "changelog"
msgid ""
"improved: smaller UI changes to current weather for better handling of long "
"condition texts"
msgstr ""

#: ../app/components/AboutChangelog.qml:149
msgctxt "changelog"
msgid "fixed: feels like string not being translated"
msgstr ""

#: ../app/components/AboutChangelog.qml:155
msgctxt "changelog"
msgid "fixed: #76 replace kph with m/s as default wind speed unit unit"
msgstr ""

#: ../app/components/AboutChangelog.qml:156
msgctxt "changelog"
msgid ""
"added: \"feels like\" temperature to current info (if available at your "
"location)"
msgstr ""

#: ../app/components/AboutChangelog.qml:157
msgctxt "changelog"
msgid "added: rain volume to hourly data and day extended info section"
msgstr ""

#: ../app/components/AboutChangelog.qml:158
msgctxt "changelog"
msgid "added: setting for rain volume unit"
msgstr ""

#: ../app/components/AboutChangelog.qml:159
msgctxt "changelog"
msgid "added: m/s as unit for wind speed (official SI unit)"
msgstr ""

#: ../app/components/AboutChangelog.qml:160
msgctxt "changelog"
msgid "improved: restructure internal unit handling and default unit values"
msgstr ""

#: ../app/components/AboutChangelog.qml:166
msgctxt "changelog"
msgid ""
"fixed: #73 locations and location search now uses localized city names (if "
"available at geonames.org). Existing locations need to be removed and added "
"again to use the localized name."
msgstr ""

#: ../app/components/AboutChangelog.qml:167
msgctxt "changelog"
msgid ""
"fixed: #74 predefined list of cities now contains cities with more than 5 "
"million inhabitants (according to geonames.org data)"
msgstr ""

#: ../app/components/AboutChangelog.qml:168
msgctxt "changelog"
msgid ""
"fixed: #46 animation when changing locations from indicator or location list"
msgstr ""

#: ../app/components/AboutChangelog.qml:169
msgctxt "changelog"
msgid "improved: redesign of location search to always show search bar"
msgstr ""

#: ../app/components/AboutChangelog.qml:176
msgctxt "changelog"
msgid "fixed: #64 mismatch of icon and text for current weather condition"
msgstr ""

#: ../app/components/AboutChangelog.qml:177
msgctxt "changelog"
msgid "fixed: #72 wrong sunrise and sunset times in places with daylightsaving"
msgstr ""

#: ../app/components/AboutChangelog.qml:178
msgctxt "changelog"
msgid "added: about page including credits"
msgstr ""

#: ../app/components/AboutChangelog.qml:179
msgctxt "changelog"
msgid ""
"improved: remapped icons for cloud and rain to show more separate conditions"
msgstr ""

#: ../app/components/AboutChangelog.qml:192
msgctxt "changelog"
msgid ""
"removed: color select option for highlight today, provide grey or none for "
"highlighting"
msgstr ""

#: ../app/components/AboutChangelog.qml:198
msgctxt "changelog"
msgid "fixed: #59 option to set background color for todays weather info"
msgstr ""

#: ../app/components/AboutChangelog.qml:199
msgctxt "changelog"
msgid "added: show pressure in extended information section"
msgstr ""

#: ../app/components/AboutChangelog.qml:200
msgctxt "changelog"
msgid "removed: setting to toggle pressure information"
msgstr ""

#: ../app/components/AboutChangelog.qml:201
msgctxt "changelog"
msgid "removed: dividers for a consistent appwide appearance"
msgstr ""

#: ../app/components/AboutChangelog.qml:219
msgctxt "changelog"
msgid "improved: use themed colors for blue and red"
msgstr ""

#: ../app/components/AboutChangelog.qml:220
msgctxt "changelog"
msgid "improved: some small UI changes to rain radar icon and header"
msgstr ""

#: ../app/components/AboutChangelog.qml:227
msgctxt "changelog"
msgid "fixed: settings page not showing correct version number"
msgstr ""

#: ../app/components/AboutChangelog.qml:228
msgctxt "changelog"
msgid "improved: new icons for rain radar map to make usage more intuitive"
msgstr ""

#: ../app/components/AboutChangelog.qml:234
msgctxt "changelog"
msgid "fixed: button margins in networkError page"
msgstr ""

#: ../app/components/AboutChangelog.qml:235
msgctxt "changelog"
msgid "added: weather radar provided by rainviewer.com (many thanks!)"
msgstr ""

#: ../app/components/AboutChangelog.qml:241
msgctxt "changelog"
msgid ""
"fixed: #66 sunrise and sunset times off by one hour in areas with daylight "
"saving"
msgstr ""

#: ../app/components/AboutChangelog.qml:248
msgctxt "changelog"
msgid ""
"fixed: temperature color setting shows the current selected value as subtitle"
msgstr ""

#: ../app/components/AboutChangelog.qml:255
msgctxt "changelog"
msgid "fixed: #63 theme not applied to non colored temperatures"
msgstr ""

#: ../app/components/AboutChangelog.qml:268
msgctxt "changelog"
msgid ""
"improved: exchange moon emojis with svg-graphics (many thanks to @cibersheep "
"for the artwork!)"
msgstr ""

#: ../app/components/AboutChangelog.qml:274
msgctxt "changelog"
msgid "fixed: #61 adjust spacing between daily and hourly forecast"
msgstr ""

#: ../app/components/AboutChangelog.qml:275
msgctxt "changelog"
msgid "added: blue/yellow color theme for high/low temperatures"
msgstr ""

#: ../app/components/AboutChangelog.qml:276
msgctxt "changelog"
msgid "improved: reduze size of dots to match app scopes"
msgstr ""

#: ../app/components/AboutChangelog.qml:283
msgctxt "changelog"
msgid ""
"fixed: #44 finish weather app redesign\n"
"* fixed: #54 make colors more consistent\n"
"* added: page indicator if more than one location is set\n"
"* added: up/down icon to day details\n"
"* improved: hourly forecast always visible"
msgstr ""

#: ../app/components/AboutChangelog.qml:284
msgctxt "changelog"
msgid ""
"fixed: #55 spacing/height of moonphase text, moon emoji infront of "
"description"
msgstr ""

#: ../app/components/AboutChangelog.qml:285
msgctxt "changelog"
msgid "fixed: #56 bottom edge not working on theme change"
msgstr ""

#: ../app/components/AboutChangelog.qml:286
msgctxt "changelog"
msgid "fixed: bad alignment on daily forecast and locations"
msgstr ""

#: ../app/components/AboutChangelog.qml:287
msgctxt "changelog"
msgid "fixed: #57 add offline mode and network available indicator"
msgstr ""

#: ../app/components/AboutChangelog.qml:288
msgctxt "changelog"
msgid "added: pressure data values"
msgstr ""

#: ../app/components/AboutChangelog.qml:289
msgctxt "changelog"
msgid "added: country name to location in location list"
msgstr ""

#: ../app/components/AboutChangelog.qml:290
msgctxt "changelog"
msgid "added: allow refresh intervals of up to 4 hours"
msgstr ""

#: ../app/components/AboutChangelog.qml:291
msgctxt "changelog"
msgid "added: info page for \"hidden\" functions in locations page"
msgstr ""

#: ../app/components/AboutChangelog.qml:292
msgctxt "changelog"
msgid "improved: settings page only flickable if needed"
msgstr ""

#: ../app/components/AboutChangelog.qml:305
msgctxt "changelog"
msgid "fixed: small visibility issue in settings page"
msgstr ""

#: ../app/components/AboutChangelog.qml:311
msgctxt "changelog"
msgid "fixed: #51 add option to use system theme"
msgstr ""

#: ../app/components/AboutChangelog.qml:312
msgctxt "changelog"
msgid ""
"fixed: #53 sunset/sunrise times now shown in their local time (thanks "
"Lorenzo)"
msgstr ""

#: ../app/components/AboutChangelog.qml:313
msgctxt "changelog"
msgid "added: icon for expanding/collapsing daily details page"
msgstr ""

#: ../app/components/AboutChangelog.qml:314
msgctxt "changelog"
msgid "improved: add wordwrap for long moonphase strings due to translation"
msgstr ""

#: ../app/components/AboutChangelog.qml:326
msgctxt "changelog"
msgid "improved: small internal tweak"
msgstr ""

#: ../app/components/AboutChangelog.qml:332
msgctxt "changelog"
msgid "added: moonphase emojis for visualization"
msgstr ""

#: ../app/components/AboutChangelog.qml:333
msgctxt "changelog"
msgid "improved: small improvements to moonphase calculations"
msgstr ""

#: ../app/components/AboutChangelog.qml:334
msgctxt "changelog"
msgid "improved: omit 0% humidity values (forecast only available for 3 days)"
msgstr ""

#: ../app/components/AboutChangelog.qml:335
msgctxt "changelog"
msgid "updated: suncalc.js from its repo"
msgstr ""

#: ../app/components/AboutChangelog.qml:341
msgctxt "changelog"
msgid "fixed: missing api key"
msgstr ""

#: ../app/components/AboutChangelog.qml:347
msgctxt "changelog"
msgid "fixed: #48 corrected sunrise and sunset times calculation"
msgstr ""

#: ../app/components/AboutChangelog.qml:348
msgctxt "changelog"
msgid "added: moonphase information"
msgstr ""

#: ../app/components/AboutChangelog.qml:354
msgctxt "changelog"
msgid "added: weekday and calendar date show in week view"
msgstr ""

#: ../app/components/AboutChangelog.qml:360
msgctxt "changelog"
msgid "added: new dark mode toggle"
msgstr ""

#: ../app/components/AboutChangelog.qml:361
msgctxt "changelog"
msgid "improved: redesigned setting page"
msgstr ""

#: ../app/components/AboutChangelog.qml:367
msgctxt "changelog"
msgid "added: dark theme support"
msgstr ""

#: ../app/components/AboutChangelog.qml:368
msgctxt "changelog"
msgid "improved: redesigned icons"
msgstr ""

#: ../app/components/AboutChangelog.qml:369
msgctxt "changelog"
msgid "improved: the app can now be rotated to a landscape view"
msgstr ""

#: ../app/components/AboutChangelog.qml:370
msgctxt "changelog"
msgid "improved: splash screen"
msgstr ""

#: ../app/components/AboutChangelog.qml:382
msgctxt "changelog"
msgid ""
"removed: Temporarily removed \"The Weather Channel\" data provider. UBports "
"only has an api key for OpenWeatherMap at this time. We apologize for the "
"inconvenience. We hope to add the option to input your own api key for "
"different services in the future."
msgstr ""

#: ../app/components/AboutChangelog.qml:394
msgctxt "changelog"
msgid "not documented, please check commit history in the repo"
msgstr ""

#: ../app/components/AboutGeneral.qml:73
#, fuzzy
msgid "Weather app"
msgstr "آب و هوا"

#: ../app/components/AboutGeneral.qml:76
msgid "Version %1"
msgstr ""

#: ../app/components/AboutGeneral.qml:91
msgid "Links"
msgstr ""

#: ../app/components/AboutGeneral.qml:103
msgid "Get the sourcecode"
msgstr ""

#: ../app/components/AboutGeneral.qml:104
msgid "Report issues"
msgstr ""

#: ../app/components/AboutGeneral.qml:105
msgid "Help translate"
msgstr ""

#: ../app/components/AboutGeneral.qml:130
msgid "Credits"
msgstr ""

#: ../app/components/AboutGeneral.qml:143
#, fuzzy
msgid "Weather data by"
msgstr "آب و هوا"

#: ../app/components/AboutGeneral.qml:149
msgid "Rainradar by"
msgstr ""

#: ../app/components/AboutGeneral.qml:155
msgid "Coordinate lookup by"
msgstr ""

#: ../app/components/AboutGeneral.qml:161
msgid "Location data lookup by"
msgstr ""

#: ../app/components/AboutGeneral.qml:167
#, fuzzy
msgid "Sun and moon calculations by"
msgstr "روی یک مکان ضربه بزنید و نگه دارید"

#: ../app/components/AboutGeneral.qml:173
msgid "Timezone converting by"
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:54
msgid ""
"If you encounter problems, please go through the following steps and try "
"again:"
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:69
#, fuzzy
msgid "1. Restart the app"
msgstr "آب و هوا"

#: ../app/components/AboutTroubleshooting.qml:70
msgctxt "troubleshooting, description how to restart the app"
msgid "Close the app, then restart it as usual."
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:71
#, fuzzy
msgid "Close weather app"
msgstr "آب و هوا"

#: ../app/components/AboutTroubleshooting.qml:75
msgid "2. Reboot the device"
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:76
msgctxt "troubleshooting, description how to reboot"
msgid ""
"Press and hold the power button to raise the system dialog and select "
"'reboot' there. Alternatively use the same option from the system indicator."
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:77
msgid "Reboot device"
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:81
#, fuzzy
msgid "3. Clear the cache"
msgstr "آب و هوا"

#: ../app/components/AboutTroubleshooting.qml:82
msgctxt "troubleshooting, description how to clear the cache"
msgid ""
"Clear the cache with the button below, use the app UT Tweak Tool or delete "
"the content of the <i>.cache/com.ubuntu.weather</i> folder."
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:83
msgid "Clear cache now"
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:87
msgid "4. Check the log"
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:88
msgctxt "troubleshooting, description how to get the log and open an issue"
msgid ""
"If the steps 1 to 3 did not solve the problem, please open an issue and "
"provide the app log. One way to obtain the log is with Logviewer app. This "
"app allows uploading the log to a pastebin. Provide the link to the pasted "
"log in the error report. Another option is to manually copy the log file. It "
"can be found under <i>./cache/upstart/application-click-com.ubuntu."
"weather_weather_VERSION.log</i>."
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:93
msgid "5. Open an issue on gitlab"
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:94
msgctxt "troubleshooting, description how to open an issue"
msgid ""
"Please go to <i>https://gitlab.com/ubports/apps/weather-app/-/issues</i> and "
"file an error report there. Please provide the following information: the "
"version of the app, the name of your device, your release channel, a "
"description of what you did and which problems you encountered."
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:95
msgid "Open issue tracker"
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:172
msgid "Cache cleared."
msgstr ""

#: ../app/components/AboutTroubleshooting.qml:180
#: ../app/components/NetworkErrorStateComponent.qml:64
#: ../app/ui/AddLocationPage.qml:318
msgid "OK"
msgstr "قبول"

#: ../app/components/AboutUsage.qml:51
msgid "Access weather information"
msgstr ""

#: ../app/components/AboutUsage.qml:52
msgctxt "usage, access weather information"
msgid ""
"You can swipe through locations or pick one by tapping on the indicator "
"dots. Tap on current information or a daily forecast for additional weather "
"details. Swipe the 3 hour forecast for more weather information."
msgstr ""

#: ../app/components/AboutUsage.qml:55
#, fuzzy
msgid "Manage locations"
msgstr "موقعیت‌ها"

#: ../app/components/AboutUsage.qml:56
msgctxt "usage, manage locations"
msgid ""
"Add multiple locations via bottom edge or enable live GPS location in "
"settings. Manage locations via bottom edge locations page by swiping or "
"press & hold to remove and reorder."
msgstr ""

#: ../app/components/AboutUsage.qml:59
#, fuzzy
msgid "Locations page list actions"
msgstr "اقدامات موجود در لیست"

#: ../app/components/AboutUsage.qml:60
msgctxt "usage, manage locations"
msgid "To add new locations you need to have internet access!"
msgstr ""

#: ../app/components/AboutUsage.qml:60
msgid "a) enable sorting mode (drag the icon to reorder)"
msgstr ""

#: ../app/components/AboutUsage.qml:60
msgid "b) multiselect locations to delete"
msgstr ""

#: ../app/components/AboutUsage.qml:60
msgid "single tap a location to view its weather"
msgstr "برای مشاهده آب و هوا، روی یک مکان ضربه بزنید"

#: ../app/components/AboutUsage.qml:60
msgid "tap and hold on a location to"
msgstr "روی یک مکان ضربه بزنید و نگه دارید"

#: ../app/components/AboutUsage.qml:63
msgid "Rainradar"
msgstr ""

#: ../app/components/AboutUsage.qml:64
msgctxt "usage, rainradar"
msgid ""
"You can view live rainradar by tapping the satellite icon in the header."
msgstr ""

#: ../app/components/AboutUsage.qml:67
msgid "GPS live location"
msgstr ""

#: ../app/components/AboutUsage.qml:68
msgctxt "usage, GPS live location"
msgid ""
"When GPS for current location is enabled, an additional 'live' location is "
"added to the list. This locations updates it's position on the move "
"according to GPS location."
msgstr ""

#: ../app/components/AboutUsage.qml:71
msgid "Updating data"
msgstr ""

#: ../app/components/AboutUsage.qml:72
msgctxt "usage, Updating data"
msgid ""
"Weather station data is updated on app start and at the interval specified "
"in the app settings. This does only work while the app is active."
msgstr ""

#: ../app/components/DayDelegateExtraInfo.qml:51
msgid "Chance of precipitation"
msgstr "احتمال بارش"

#: ../app/components/DayDelegateExtraInfo.qml:61
msgid "Rain volume"
msgstr ""

#: ../app/components/DayDelegateExtraInfo.qml:68
msgid "Snow volume"
msgstr ""

#: ../app/components/DayDelegateExtraInfo.qml:75
msgid "Winds"
msgstr "باد"

#: ../app/components/DayDelegateExtraInfo.qml:99
msgid "Humidity"
msgstr "رطوبت"

#: ../app/components/DayDelegateExtraInfo.qml:106
msgid "Sunrise"
msgstr "طلوع"

#: ../app/components/DayDelegateExtraInfo.qml:113
msgid "Sunset"
msgstr "غروب"

#: ../app/components/DayDelegateExtraInfo.qml:120
msgid "Pressure"
msgstr ""

#: ../app/components/DayDelegateExtraInfo.qml:127
msgid "Moonphase"
msgstr "حالت ماه"

#: ../app/components/HeadState/LocationsHeadState.qml:30
#: ../app/components/HeadState/MultiSelectHeadState.qml:31
#: ../app/ui/HomePage.qml:76
msgid "Locations"
msgstr "موقعیت‌ها"

#: ../app/components/HeadState/MultiSelectHeadState.qml:36
msgid "exit reorder mode"
msgstr "از حالت تغییر ترتیب خارج شوید"

#: ../app/components/HeadState/MultiSelectHeadState.qml:47
#, fuzzy
msgctxt "location list page, select all button description"
msgid "Select All"
msgstr "انتخاب همه"

#: ../app/components/HeadState/MultiSelectHeadState.qml:47
#, fuzzy
msgctxt "location list page, unselect all button description"
msgid "Unselect All"
msgstr "انتخاب همه"

#: ../app/components/HeadState/MultiSelectHeadState.qml:59
msgid "Delete"
msgstr "حذف"

#: ../app/components/HomeHourly.qml:177
#, javascript-format
msgctxt "chance of precipitation, hourly forecast"
msgid "%1%"
msgstr ""

#: ../app/components/HomePageEmptyStateComponent.qml:51
msgid "Searching for current location..."
msgstr "در حال جست‌وجو برای موقعیّت جاری…"

#: ../app/components/HomePageEmptyStateComponent.qml:52
msgid "Cannot determine your location"
msgstr "نمي‌توان موقعیّتتان را تعیین کرد"

#: ../app/components/HomePageEmptyStateComponent.qml:62
msgid "Manually add a location by swiping up from the bottom of the display"
msgstr "با لغزش به بالا از پایین نمایشگر، موقعیّتی را به صورت دستی بیفزایید"

#: ../app/components/ListItemActions/Remove.qml:26
msgid "Remove"
msgstr "برداشتن"

#: ../app/components/LocationsListPageEmptyStateComponent.qml:36
msgid "No locations found. Tap the plus icon to search for one."
msgstr ""
"هیج موقعیّتی پیدا نشد. برای جست‌وجوی یک موقعیّت، روی نقشک مثبت صربه بزنید."

#: ../app/components/LocationsListPageEmptyStateComponent.qml:36
#, fuzzy
msgid ""
"No network connection.\n"
"Unable to add new locations.\n"
"Please check your network settings and try again."
msgstr "لطفاً تنظیمات شبکه‌تان را بررسی کرده و دوباره تلاش کنید."

#: ../app/components/NetworkErrorStateComponent.qml:49
msgid "Network Error"
msgstr "خطای شبکه"

#: ../app/components/NetworkErrorStateComponent.qml:58
#, fuzzy
msgid ""
"No network connection. Could not load weather data.\n"
"Please check your network settings and try again."
msgstr "لطفاً تنظیمات شبکه‌تان را بررسی کرده و دوباره تلاش کنید."

#: ../app/components/NoAPIKeyErrorStateComponent.qml:47
msgid "No API Keys Found"
msgstr "هیچ کلید رابط برنامه‌نویسی‌ای پیدا نشد"

#: ../app/components/NoAPIKeyErrorStateComponent.qml:56
msgid ""
"If you are a developer, please see the README file for instructions on how "
"to obtain your own Open Weather Map API key."
msgstr ""
"لطفاً اگر توسعه‌دهنده هستید، برای دستورالعمل‌ها دربارهٔ چگونگی کسب کلید رابط "
"برنامه‌نویسی Open Weather Map خود، پروندهٔ README را ببینید."

#: ../app/components/OnlineMap.qml:37
msgid "Rain radar"
msgstr ""

#: ../app/data/moonphase.js:13
msgid "New moon"
msgstr ""

#: ../app/data/moonphase.js:17
msgid "Waxing Crescent"
msgstr ""

#: ../app/data/moonphase.js:21
msgid "First Quarter"
msgstr ""

#: ../app/data/moonphase.js:25
msgid "Waxing Gibbous"
msgstr ""

#: ../app/data/moonphase.js:29
msgid "Full moon"
msgstr ""

#: ../app/data/moonphase.js:33
msgid "Waning Gibbous"
msgstr ""

#: ../app/data/moonphase.js:37
msgid "Last Quarter"
msgstr ""

#: ../app/data/moonphase.js:41
msgid "Waning Crescent"
msgstr ""

#: ../app/data/moonphase.js:45
msgid "calculation error"
msgstr ""

#. TRANSLATORS: %1 does specify the parameters, %2 does specify the url for weather apps repo at gitlab
#: ../app/ubuntu-weather-app.qml:406
msgid ""
"Valid arguments for weather app are: %1 They will be managed by system. See "
"the README at %2 for a full comment about them"
msgstr ""

#: ../app/ui/AddLocationPage.qml:41
msgid "Back"
msgstr "بازگشت"

#: ../app/ui/AddLocationPage.qml:83
msgid "Search city or select below"
msgstr ""

#: ../app/ui/AddLocationPage.qml:292
msgid "No city found"
msgstr "هیچ شهری پیدا نشد"

#: ../app/ui/AddLocationPage.qml:305
msgid "Couldn't load weather data, please try later again!"
msgstr "نمی‌توان داده‌های آب‌وخوا را بار کرد، لطفاً بعداً دوباره تلاش نمایید!"

#: ../app/ui/AddLocationPage.qml:315
msgid "Location already added."
msgstr "موقعیت قبلا افزوده شده است."

#. TRANSLATORS: N = North, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:123
msgid "N"
msgstr ""

#. TRANSLATORS: NE = North East, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:126
msgid "NE"
msgstr ""

#. TRANSLATORS: E = East, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:129
msgid "E"
msgstr ""

#. TRANSLATORS: SE = South East, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:132
msgid "SE"
msgstr ""

#. TRANSLATORS: S = South, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:135
msgid "S"
msgstr "س"

#. TRANSLATORS: SW = South West, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:138
msgid "SW"
msgstr ""

#. TRANSLATORS: W = West, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:141
msgid "W"
msgstr ""

#. TRANSLATORS: NW = NorthWest, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:144
msgid "NW"
msgstr ""

#. TRANSLATORS: temperatures in °F, keep/remove the space before the unit according to your language specifications
#: ../app/ui/HomePage.qml:201
msgid "%1 °F"
msgstr ""

#. TRANSLATORS: temperatures in °C, keep/remove the space before the unit according to your language specifications
#: ../app/ui/HomePage.qml:207
msgid "%1 °C"
msgstr ""

#. TRANSLATORS: wind speed in m/s, keep/remove the space before the unit according to your language specifications
#: ../app/ui/HomePage.qml:220
msgid "%1 m/s"
msgstr ""

#. TRANSLATORS: wind speed in km/h, keep/remove the space before the unit according to your language specifications
#: ../app/ui/HomePage.qml:224
msgid "%1 km/h"
msgstr ""

#. TRANSLATORS: wind speed in mph, keep/remove the space before the unit according to your language specifications
#: ../app/ui/HomePage.qml:228
msgid "%1 mph"
msgstr ""

#. TRANSLATORS: rain/snow volume in inch, keep/remove the space before the unit according to your language specifications
#. TRANSLATORS: rain/snow volume in litre per squaremeter, keep/remove the space before the unit according to your language specifications
#: ../app/ui/HomePage.qml:251 ../app/ui/HomePage.qml:254
msgid "%1 in"
msgstr ""

#. TRANSLATORS: rain/snow volume in millimeter, keep/remove the space before the unit according to your language specifications
#: ../app/ui/HomePage.qml:257
msgid "%1 mm"
msgstr ""

#: ../app/ui/LocationPane.qml:60 ../app/ui/LocationPane.qml:111
msgid "Today"
msgstr "امروز"

#: ../app/ui/LocationPane.qml:115
msgid "updated %1 day ago"
msgid_plural "updated %1 days ago"
msgstr[0] ""
msgstr[1] ""

#: ../app/ui/LocationPane.qml:117
msgid "updated %1 hour ago"
msgid_plural "updated %1 hours ago"
msgstr[0] ""
msgstr[1] ""

#: ../app/ui/LocationPane.qml:119
msgid "updated %1 minute ago"
msgid_plural "updated %1 minutes ago"
msgstr[0] ""
msgstr[1] ""

#: ../app/ui/LocationPane.qml:121
msgid "updated recently"
msgstr ""

#. TRANSLATORS: % (percent) is the unit for humidity or chance or precipitation, add a space before the unit according to your language specifications
#: ../app/ui/LocationPane.qml:291
#, javascript-format
msgctxt "humidity, daily forecast"
msgid "%1%"
msgstr ""

#. TRANSLATORS: hectopascal, unit for air pressure, only use abbreviated
#: ../app/ui/LocationPane.qml:298
#, fuzzy
msgctxt "pressure, daily forecast"
msgid "%1 hPa"
msgstr "1% ساعت ها"

#. TRANSLATORS: first value is wind speed, second value is wind bearing, reorder and keep/remove space as suitable
#: ../app/ui/LocationPane.qml:303
msgctxt "wind data, daily forecast"
msgid "%1 %2"
msgstr ""

#. TRANSLATORS: % (percent) is the unit for humidity or chance or precipitation, add a space before the unit according to your language specifications
#: ../app/ui/LocationPane.qml:306
#, javascript-format
msgctxt "chance of precipitation, daily forecast"
msgid "%1%"
msgstr ""

#. TRANSLATORS: feels like refers to the current temperature as how it feels adjusted e.g. by wind
#: ../app/ui/LocationPane.qml:333
msgctxt "current weather"
msgid "feels like: %1"
msgstr ""

#. TRANSLATORS: %1 is cloud coverage in percent, keep/remove the space before the unit according to your language specifications
#: ../app/ui/LocationPane.qml:335
#, fuzzy
msgctxt "current weather"
msgid "Clouds: %1 %"
msgstr "رطوبت"

#. TRANSLATORS: %1 is wind speed including unit and %2 is wind direction
#: ../app/ui/LocationPane.qml:337
#, fuzzy
msgctxt "current weather"
msgid "Wind: %1 %2"
msgstr "رطوبت"

#. TRANSLATORS: humidity in percent, keep/remove the space before the unit according to your language specifications
#: ../app/ui/LocationPane.qml:339
#, fuzzy
msgctxt "humidity, current weather"
msgid "Humidity: %1 %"
msgstr "رطوبت"

#: ../app/ui/LocationsListPage.qml:107
msgid "Current Location"
msgstr "موقعیّت جاری"

#: ../app/ui/SettingsPage.qml:28
msgid "Settings"
msgstr "تنظیمات"

#: ../app/ui/settings/DataProviderPage.qml:34
msgid "Data Provider"
msgstr "فراهم‌کنندهٔ داده"

#: ../app/ui/settings/DataProviderPage.qml:35
#, fuzzy
msgid "OpenWeatherMap"
msgstr "آب و هوا"

#: ../app/ui/settings/LocationPage.qml:32
msgid "Detect current location"
msgstr "تشخیص موقعیّت جاری"

#. TRANSLATORS: millimeter, metric unit for rain/snow given as millimeter per hour, only use abbreviated
#: ../app/ui/settings/RainUnitsPage.qml:32
msgid "mm"
msgstr ""

#. TRANSLATORS: inch, imperial unit for rain/snow given as inch per hour, only use abbreviated
#: ../app/ui/settings/RainUnitsPage.qml:35
msgid "in"
msgstr ""

#. TRANSLATORS: liter, metric unit for rain/snow given as liter per squaremeter per hour, only use abbreviated
#: ../app/ui/settings/RainUnitsPage.qml:38
msgid "l/m²"
msgstr ""

#: ../app/ui/settings/RainUnitsPage.qml:48
msgid "Rain volume unit"
msgstr ""

#: ../app/ui/settings/RefreshIntervalPage.qml:31
#: ../app/ui/settings/RefreshIntervalPage.qml:32
#: ../app/ui/settings/RefreshIntervalPage.qml:33
#: ../app/ui/settings/RefreshIntervalPage.qml:45
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "1% دقیقه"

#: ../app/ui/settings/RefreshIntervalPage.qml:34
#: ../app/ui/settings/RefreshIntervalPage.qml:35
#: ../app/ui/settings/RefreshIntervalPage.qml:36
#: ../app/ui/settings/RefreshIntervalPage.qml:45
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "1% ساعت ها"

#: ../app/ui/settings/RefreshIntervalPage.qml:44
msgid "Refresh Interval"
msgstr "فاصلهٔ نوسازی"

#: ../app/ui/settings/RefreshIntervalPage.qml:46
msgid "Data is only updated while the app is open and focused"
msgstr ""

#. TRANSLATORS: degree celsius, metric unit for temperature, only use abbreviated
#: ../app/ui/settings/TempUnitsPage.qml:33
msgid "°C"
msgstr "°C"

#. TRANSLATORS: degree fahrenheit, imperial unit for temperature, only use abbreviated
#: ../app/ui/settings/TempUnitsPage.qml:36
msgid "°F"
msgstr "°F"

#: ../app/ui/settings/TempUnitsPage.qml:46
msgid "Temperature unit"
msgstr ""

#: ../app/ui/settings/TemperatureColorPage.qml:31
msgid "no colors"
msgstr ""

#: ../app/ui/settings/TemperatureColorPage.qml:32
msgid "blue/yellow"
msgstr ""

#: ../app/ui/settings/TemperatureColorPage.qml:33
msgid "blue/orange"
msgstr ""

#: ../app/ui/settings/TemperatureColorPage.qml:34
msgid "blue/red"
msgstr ""

#: ../app/ui/settings/TemperatureColorPage.qml:42
#, fuzzy
msgid "Temperature colors"
msgstr "دما"

#: ../app/ui/settings/ThemePage.qml:32
msgid "System theme"
msgstr ""

#: ../app/ui/settings/ThemePage.qml:33
msgid "SuruDark theme"
msgstr ""

#: ../app/ui/settings/ThemePage.qml:34
msgid "Ambiance theme"
msgstr ""

#: ../app/ui/settings/ThemePage.qml:51
msgid "Style"
msgstr ""

#: ../app/ui/settings/TodayColorPage.qml:33
msgid "Grey background for current data?"
msgstr ""

#. TRANSLATORS: meter per second, metric SI unit for wind speed, only use abbreviated
#: ../app/ui/settings/WindUnitsPage.qml:34
msgid "m/s"
msgstr ""

#. TRANSLATORS: kilometer per hour, metric unit for wind speed, only use abbreviated
#: ../app/ui/settings/WindUnitsPage.qml:37
msgid "km/h"
msgstr ""

#. TRANSLATORS: miles per hour, imperial unit for wind speed, only use abbreviated
#: ../app/ui/settings/WindUnitsPage.qml:40
msgid "mph"
msgstr "مل/س"

#: ../app/ui/settings/WindUnitsPage.qml:50
msgid "Wind speed unit"
msgstr ""

#: ubuntu-weather-app.desktop.in.in.h:1
msgid "Weather"
msgstr "آب و هوا"

#: ubuntu-weather-app.desktop.in.in.h:2
#, fuzzy
msgid "A weather forecast application for Ubuntu Touch"
msgstr ""
"یک برنامه‌ی پیش‌بینی آب‌وهوا برای اوبونتو که از چند منبع داده‌ی آب‌وهوایی "
"پشتیبانی می‌کند"

#: ubuntu-weather-app.desktop.in.in.h:3
#, fuzzy
msgid "weather;forecast;openweathermap;sunrise;sunset;moonphase;humidity;wind;"
msgstr "آب‌وهوا;پیش‌بینی;twc;openweathermap;کانال آب‌وهوا;"

#, fuzzy
#~ msgid "Wind"
#~ msgstr "باد"

#~ msgid "UV Index"
#~ msgstr "شاخص ماواری بنفش"

#~ msgid "Pollen"
#~ msgstr "ریزگرد"

#, fuzzy
#~ msgid "Temperature Unit"
#~ msgstr "دما"

#~ msgid "kph"
#~ msgstr "ک‌م/س"

#, fuzzy
#~ msgid "Wind Speed Unit"
#~ msgstr "سرعت باد"

#~ msgid "Select a city"
#~ msgstr "گزینش یک شهر"

#~ msgid "City"
#~ msgstr "شهر"

#~ msgid "Search city"
#~ msgstr "جست‌وجوی شهر"

#~ msgid "Cancel selection"
#~ msgstr "لغو گزینش"

#~ msgid "Retry"
#~ msgstr "تلاش دوباره"

#~ msgid "Units"
#~ msgstr "واحدها"

#~ msgid "Location"
#~ msgstr "موقعیّت"

#~ msgid "Provider"
#~ msgstr "فراهم‌کننده"

#~ msgid "Interval"
#~ msgstr "بازه"
