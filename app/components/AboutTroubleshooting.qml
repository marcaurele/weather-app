/*
 * Copyright (C) 2021 UBports
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3 as UC //for Dialogs
import io.thp.pyotherside 1.4 //for Python
import Qt.labs.folderlistmodel 2.1 //for FolderListModel
import Qt.labs.platform 1.0 //for StandardPaths

Page {
    id: aboutUsagePage

    property var unitySessionService: DBusUnitySessionService

    Flickable {
        id: usageFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors {
            topMargin: units.gu(2)
            fill: parent
        }

        contentHeight: usageColumn.childrenRect.height

        Column {
            id: usageColumn

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            Label {
                id: headlineLinksLabel
                text: i18n.tr("If you encounter problems, please go through the following steps and try again:") + "\n"
                wrapMode: Text.WordWrap
                anchors {
                    left: parent.left
                    leftMargin: units.gu(2)
                    right: parent.right
                    rightMargin: units.gu(2)
                }
            }

            Repeater {
                id: listViewUsage

                model: [
                    {
                        title: i18n.tr("1. Restart the app"),
                        description: i18n.ctr("troubleshooting, description how to restart the app","Close the app, then restart it as usual."),
                        buttonText: i18n.tr("Close weather app"),
                        buttonAction: "quitAction"
                    },
                    {
                        title: i18n.tr("2. Reboot the device"),
                        description: i18n.ctr("troubleshooting, description how to reboot","Press and hold the power button to raise the system dialog and select 'reboot' there. Alternatively use the same option from the system indicator."),
                        buttonText: i18n.tr("Reboot device"),
                        buttonAction: "" //"rebootAction"
                    },
                    {
                        title: i18n.tr("3. Clear the cache"),
                        description: i18n.ctr("troubleshooting, description how to clear the cache","Clear the cache with the button below, use the app UT Tweak Tool or delete the content of the <i>.cache/com.ubuntu.weather</i> folder."),
                        buttonText: i18n.tr("Clear cache now"),
                        buttonAction: "clearCacheAction"
                    },
                    {
                        title: i18n.tr("4. Check the log"),
                        description: i18n.ctr("troubleshooting, description how to get the log and open an issue","If the steps 1 to 3 did not solve the problem, please open an issue and provide the app log. One way to obtain the log is with Logviewer app. This app allows uploading the log to a pastebin. Provide the link to the pasted log in the error report. Another option is to manually copy the log file. It can be found under <i>./cache/upstart/application-click-com.ubuntu.weather_weather_VERSION.log\</i>."),
                        buttonText: "",//i18n.tr("Share log"),
                        buttonAction: "" //"openLogAction"
                    },
                    {
                        title: i18n.tr("5. Open an issue on gitlab"),
                        description: i18n.ctr("troubleshooting, description how to open an issue","Please go to <i>https://gitlab.com/ubports/apps/weather-app/-/issues</i> and file an error report there. Please provide the following information: the version of the app, the name of your device, your release channel, a description of what you did and which problems you encountered."),
                        buttonText: i18n.tr("Open issue tracker"),
                        buttonAction: "openGitAction"
                    }
                ]

                delegate: Column {
                    id: delegateColumn
                    height: childrenRect.height
                    width: parent.width - units.gu(4)
                    spacing: units.gu(1)
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(2)
                        right: parent.right
                        rightMargin: units.gu(2)
                    }
                    Label {
                        id: titleLabel
                        text: modelData.title
                        font.bold: true
                    }
                    Label {
                        id: descriptionLabel
                        visible: modelData.description != ""
                        width: parent.width
                        text: modelData.description
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignJustify
                    }
                    Button{
                        text: modelData.buttonText
                        visible: modelData.buttonAction != ""
                        color: {
                            if (modelData.buttonAction != "openGitAction") {
                                return theme.palette.normal.negative
                            } else return theme.palette.normal.activity
                        }
                        onClicked: {
                            if (modelData.buttonAction === "quitAction") {
                                Qt.quit()
                            } else if (modelData.buttonAction === "rebootAction") {
                                //currently there is no confined way to reboot the device, so this will stay unimplemented for now
                            } else if (modelData.buttonAction === "clearCacheAction") {
                                //delete the cached files one by one using the python os module
                                //we could just delete the folder, but some cached files are used while the app is running
                                //by deleting files one by one there should be a write lock on used files
                                for (var i = 0; i < folderModel.count; i ++) {
                                  py.call('os.remove', [folderModel.get (i, "fileURL").toString().replace("file://","")], function (result) {
                                      //TODO: add error handling
                                  });
                                }
                                PopupUtils.open(cacheClearedDialog)
                            } else if (modelData.buttonAction === "openLogAction") {
                                //TODO: implement log action
                            } else if (modelData.buttonAction === "openGitAction") {
                                Qt.openUrlExternally("https://gitlab.com/ubports/apps/weather-app/-/issues")
                            }
                        }
                    }
                    Rectangle {
                        id: delegateSpacer
                        width: parent.width
                        height: units.gu(2)
                        color: "transparent"
                    }
                }
            }
        }
    }

    property string messagetext

    Component {
        id: cacheClearedDialog

        UC.Dialog {
            id: cacheDialog
            title: i18n.tr("Cache cleared.")

            Label {
                id: messageLabel
                text: messagetext
            }

            Button {
                text: i18n.tr("OK")
                onClicked: PopupUtils.close(cacheDialog)
            }
        }
    }

    //initiate a Python component for calls to Python commands
    Python {
      id: py

      Component.onCompleted: {
          //this works as the import statement in a python script
          importModule('os', function() { console.log("DEBUG: python loaded"); });
      }
    }
    //used to read all files from the apps .cache folder
    FolderListModel {
      id: folderModel
      folder: StandardPaths.writableLocation(StandardPaths.CacheLocation) + "/qmlcache"
      nameFilters: ["*.qmlc","*.jsc"]
    }

}
