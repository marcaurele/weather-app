/*
 * Copyright (C) 2020 UBports
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

Page {
    id: aboutPage

    header: PageHeader {
        id: header
        title: i18n.tr("About")

        extension: Sections {
            id: headerSections
            width: parent.width  // needed, otherwise the sections are not horizontally swipeable
            StyleHints {sectionColor: theme.palette.normal.baseText; }
            model: [i18n.ctr("about page, section header, general app info","General"), i18n.ctr("about page, section header, usage instructions","Usage"), i18n.ctr("about page, section header, full changelog","Changelog"), i18n.ctr("about page, section header, troubleshooting","Troubleshooting")]
        }
    }

    Flickable {
        id: aboutFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        contentHeight: aboutColumn.height + units.gu(2)

        Loader {
            anchors {
                fill: parent
            }
            asynchronous: true
            source: "../components/AboutGeneral.qml"
            visible: headerSections.selectedIndex === 0
        }
        Loader {
            anchors {
                fill: parent
            }
            asynchronous: true
            source: "../components/AboutUsage.qml"
            visible: headerSections.selectedIndex === 1
        }
        Loader {
            anchors {
                fill: parent
            }
            asynchronous: true
            source: "../components/AboutChangelog.qml"
            visible: headerSections.selectedIndex === 2
        }
        Loader {
            anchors {
                fill: parent
            }
            asynchronous: true
            source: "../components/AboutTroubleshooting.qml"
            visible: headerSections.selectedIndex === 3
        }
    }
}
