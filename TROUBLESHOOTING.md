# Troubleshooting

If you encounter problems, please go through the following steps and try again:

1. Restart the app

    Close the app, then restart it as usual.

2. Reboot the device

    Press and hold the power button to raise the system dialog and select 'reboot' there. Alternatively use the same option from the system indicator.

3. Clear the cache

    Clear the cache with the app UT Tweak Tool or delete the content of the *.cache/com.ubuntu.weather* folder.

4. Check the log

    If the steps 1 to 3 did not solve the problem, please open an issue and provide the app log. One way to obtain the log is with Logviewer app. This app allows uploading the log to a pastebin. Provide the link to the pasted log in the error report. Another option is to manually copy the log file. It can be found under *./cache/upstart/application-click-com.ubuntu.weather_weather_VERSION.log*.

5. Open an issue on gitlab

    Please go to [https://gitlab.com/ubports/apps/weather-app/-/issues](https://gitlab.com/ubports/apps/weather-app/-/issues) and file an error report there. Please provide the following information:
    - the version of the app
    - the name of your device
    - your release channel
    - a description of what you did and which problems you encountered
